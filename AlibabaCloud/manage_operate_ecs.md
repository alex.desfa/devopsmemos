# ECS
> Elastic Compute Service (IaaS):
> - resizable
> - large scale
> - virtual
> - various OS
> - flexible
>
> _from OS to APP without HW concerns_

## Modules
- __Apsara__ : cluster management system operating a virtualization environnement based on Xen/KVM
- __Pangu__ : distributed storage

## Regions
14 regions with independent power and network resources, containing zones in which Virtual Machines (VM) can be instantiated:
- VM inside a same region can communicate through a __private network__.
- VM in different regions can communicate only through a __public network__.

## Instances
> A virtual computer composed of CPU, MEMORY, STORAGE and NETWORK

### Properties:
- organized in __families__ :

  ```
  ecs.sn1.3xlarge
      xx| hardware specifications
  family|
        generation      
  ```
- downtime failover monitoring: the migration will occur within the same zone
- user-defined data and scripts

### Costs:
- Pay As You Go : by seconds
- Monthly or Yearly subscription Instances
  > user can switch in between the 2 modes

# Storage
Cloud Disk available as a block storage (Pangu stores it in 3 copies)
- __Ultra Cloud__ or __SSD Cloud__
- mounted in a same region, in one instance at a time
- supports only volume mount
- __snapshots__: copy of a disc at a given time, copies are incremental, can be scheduled and planified
- __disk images__:
  - system images or public images (default)
  - marketplace images (can have a cost)
  - custom images (useful for scaling)

# Network
__VPC__: virtual network based on SDN providing VLAN isolation

> VPC can be cross-zones

- customizable: IPs, subnets, masks, etc
- __VPN__ access: physical dedicated line
- __EIP__: Elastic Public Network IP
  - one can bind/unbind an EIP to a __VPC__
- vSwitch: max 24
- vRouter: hub/gateway

# Security Group
It is assimilable as a firewall by IP(CIDR block) or by security groups (intranet only).

> SG can be cross-zones

- isolate different users (100 max SG)
