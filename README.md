# devopsmemos

Different memos refering to DevOps

## Using markdown for documentation

Using [pandoc](http://pandoc.org/MANUAL.html) we can easily convert our notes into pdf or html
It is then easy to copy/paste into a mail or to join it.

For example, conversion from md to html can be done like that :

  ```shell
  pandoc -f markdown -t html5 -s -o hyperconvergence.html hyperconvergence.md --css pandoc.css
  ```

## List of memos :
- Hyperconvergence
- Docker
    - Docker EE
- RedHat
    - Ansible
- ManageIQ
