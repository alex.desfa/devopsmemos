# Install
sudo add-apt-repository ppa:gns3/ppa
sudo apt-get update
sudo apt-get install gns3-gui

# with docker
Remove any old versions:
sudo apt-get remove docker docker-engine docker.io

Install these packages, if not already present:
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
Import the official Docker GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
Setup the Docker repo:
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
*NOTE* There currently isn’t a package for Ubuntu 18.04 in the stable release channel, but there is in the edge channel.
Update the metadata:
sudo apt-get update
Install Docker:
sudo apt-get install docker-ce
Finally, add your user to the docker group, and restart your user session
sudo usermod -a -G docker username
NOTE_1 If you encounter any permission errors when trying to run GNS3, DynaMIPS devices, Wireshark, or Qemu VMs, ensure your user belongs to the following groups:

ubridge libvirt kvm wireshark

You can see which groups your user belongs to, by simply typing “groups” in a command terminal. If your user account is missing from one or more of these groups, user the usermod command listed above to add them. Be sure to log out and back in to restart your user session

NOTE_2 If running Ubuntu in VMware with Qemu v2.8 or greater, there is an issue with VMs hanging. There’s a workaround posted at the bottom of this document. 
