# Event Hyperconvergence

## Les enjeux de l'IT
Que sera le **next generation data center** ?

> Pas de full cloud comme on le prédisait il y a 10 ans mais du multi-cloud, du cloud hybride
tourné ver la donnée, l'élément critique des data center de nouvelle génération.
L'IT doit faire face à une explosion du volume des données, devenues plus que critique, elles servent maintenant à générer des revenus, elles sont dorénavant réglementée - RGPD - et multidimensionnelles

## Les problématiques des data center

* **Réduction des équipes IT** : moins de gens formés aux nouveaux outils sont disponibles dans l'IT, recours à plus de conseil et de sous-traitance
* La **Transformation Numérique** est en marche : les entreprises se transforment pour rester compétitives, pour donner accès à leurs services de manière nouvelle
* La **Bande Passante** apparaît comme un  problème majeur des data center pour faire transiter toutes la donnée


## L'Hyper-convergence
> L'hyper-convergence est un type d'architecture informatique matérielle qui intègre de façon étroitement liée les composants de traitement, de stockage, de réseau et de virtualisation. L'hyper-convergence permet une consolidation importante au niveau des centres informatiques.
>
> Par rapport à l'infrastructure convergée, l'hyper-convergence englobe les éléments d'infrastructure dans un pool de ressources partagées, en intégrant un stockage réparti sur les différents nœuds, dédupliqué, compressé et optimisé, en positionnant les blocs les plus accédés sur du disque de type SSD et les autres sur des disques plus capacitifs. Le principe est de consolider sur une seule pile logicielle tous les composants, dans une démarche « software defined ».
>
> L'intérêt est de permettre la croissance de façon linéaire en suivant les besoins, sans remettre en cause l'architecture globale, les ressources de stockage augmentant linéairement avec les ressources processeurs, typiquement pour des infrastructures hébergeant plusieurs milliers de machines virtuelles. Cela permet de résoudre ou au moins de limiter les contentions d'entrée-sortie.

[Plus de détails sur Wikipédia](https://fr.wikipedia.org/wiki/Hyper-convergence)

### Le défi
Offrir à la fois de la simplicité, une réduction du coût global (TCO), une évolution sans risque, tout en se préparant pour l'avenir, c'est à dire en faisant preuve de flexibilité et d'agilité

## Les solutions
Les grands acteurs de l'hyper-convergence affichent une maturité dans les services et solutions qu'ils proposent au niveau :

  - du cloud privé choisi par beaucoup d'entreprises notamment pour des raisons de facilités, de compétences, d'agilité, de scalabilité, de maîtrise des dépenses,  d'innovation (shadow IT)
  - de la portabilité des services depuis le cloud privé vers le cloud public donc sur cloud hybride
  - de la sécurisation des données : réplication, déduplication, compression, restauration, etc ...


### VMware VSAN
**vCenterServer, vSphere, vSan(pool de stockage virtuel)**

Nouveauté :

- vSan déployable sur le cloud public (aws)

Atouts:

- idéal pour le renouvellement de serveurs utilisant les techno VMware
- sécurité de la data
- haute dispo des stockages

### Cisco Hyperflex
Après le rachat de Springpath, Cisco offre des solutions cloud & containers orientées sur 3 axes **Computer, storage, network**

Nouveauté:

- Solution PME/PMI **Hyperflex Edge**: économies de coûts pour 2 à 4 nœuds

Atouts:

- réseau haute performance
- déploiements rapides incluant le réseau
- UCS : serveurs Cisco Unified Computing System
- économies au niveaux des ports de Switch
- solutions hors cluster
- **InterSight** solution gratuite de gestion dans le cloud
- maîtrise hardware (cartes d'accélération de data) et software
+ Photos démo

### HPE SimpliVity:

Nouveauté:

- Rachat de Plexxi, renommée **'Composable Fabric'** : service permettant la configuration automatique des réseaux, intégrée au vCenter et compatible NSX

Atouts:

- déployable sur un noeud seulement, pas besoin de cluster (pour du dev principalement)
- robustesse du **dl380 gen 10**: carte accélératrice de réplication et de déduplication
- service important de sauvegarde, deduplication et compression par défaut, tous les nœuds ont la même information
- management unifié via la WebUI
- **RapidDR** (option d'automatisation de restauration): restauration d'1To en moins d'une minute
- protection du hardware avec une puce intégrée

### NetApp HCI
*Tout miser sur la réactivité, l'IT n'est plus un frein au business*

Atouts:

- déploiements rationalisés basés sur vmWare
- **Active IQ**: service d'Ia pour résoudre les problématiques de performances, aider aux bonnes pratiques, une application prédictive
- **Trident**: outil émergent pour la gestion du stockage des containers
- gestion fine de la performance: définition de niveaux de performance des VMs afin de mieux gérer les pics d'activité

### VxRail de Dell EMC

Nouveauté:

- **VxFlexOs**: offre la liberté de l'hyperviseur

Atouts :

- basé sur du serveur Dell en blades donc du matériel répondant aux besoins clients
- intégration native de **vSphere** et **vSan**
- solution logicielle **VxRails**: logiciels propriétaires de management, de backup, d'automatisation des installations grâce à **VxRailManager** qui gère aussi l'installation des mises à jour pendant la durée de vie du produit
- support Dell EMC avec remontée d'alertes automatique
