# Automate Everything With Ansible

## Vue d'ensemble
*Presentation par Belkacem Moussouni - Business Devlopment Lead pour la région EMA(belka@redhat.com), voir les slides [ici](Ansible_Automation_Beyond_Linux.pdf)*

Depuis fin 2018, __Ansible est dans le top 10 des projets Open Source__, très apprécié de la communauté, aussi bien sur Linux, que sur **Windows** (90+ modules) et maintenant sur les plateformes **réseau** (50+ plateformes, 700+ modules).

La version entreprise **Ansible Tower** a déjà conquis plus de 1000 clients soit un parc d'environ 2 millions de machines déployées grâce à Ansible.

Le focus de la solution entreprise à d'abord porté sur l'infrastructure (depuis le rachat en 2015), aujourd'hui le focus est sur la partie réseau et demain sur la sécurité et le stockage.

L'ecosystème **Ansible Galaxy** est en pleine croissance et la dernière nouveauté est la **certification des rôles et des modules** par les équipes Red Hat.

![Image 1](img1.jpg)
![Image 2](img2.jpg)

## Red Hat Insights
*Presentation & Demo par David Clauvel*

Red Hat Insights est un outil IT d'analyse prédictive. Il aide ses utilisateurs au travers d'une AI de type SaaS à identifier, trier et résoudre les vulnérabilités détectées sur leurs machines avant qu'elles soient compromises.

Insights est intégré à RHEL et Satellite (> 5.8), et s'intègre dans Ansible Tower grâce à un plugin.

### Que fait Insights ?

* **analyse** l'infrastructure grâce à des playbooks Ansible
* **présente des rapports** lisible permettant de gérer la sécurité d'une infrastructure
* présente le **détail des vulnérabilités** détectées afin de mieux les **prioriser**
* **génère des playbooks Ansible** de remédiation des vulnérabilités
* permet de lancer les playbooks générés sur une ou sur un groupe de machines
* permet de créer des **plans** de sécurisation

[Guide sur Ansible Tower & Insights](https://www.ansible.com/blog/easy-integration-using-ansible-tower-and-red-hat-insights)

## Ansible Tower

*Démo par Victor Da Costa - [Lien GitHub](https://github.com/victorock/loadbalance_demo)*

### Les atouts de Tower:

- full **HTML5**
- nombreux **plugins**
- **REST API** and **CLI**
- permet des gérer les **playbooks Ansible** de manière sécurisée sur un parc de machines
- **gestion d'utilisateurs** (SAML, AD, LDAP, etc) et **contrôle d'accès**
- stockage sécurisé des **credentials** d'accès aux machines et services IT & Cloud
- gère des projets hébergés sur GIT
- utilise des **templates** pour mettre en place des **jobs** planifiables et dotés de permissions
- les jobs peuvent être mis en place via des **survey** qui sont des formulaires pré-remplis permettant aux utilisateurs non administrateurs d'exécuter des playbooks
- le **job slicing** permet de dimensionner la charge sur la machine exécutant les playbooks
- **gestion d'inventaire** supportant les providers majeurs de cloud et containers, basé sur le **gather facts** d'Ansible Engine avec une intégration facile d'Insights
- les **workflows** permettent de créer des pipelines de jobs qui peuvent être imbriqués, conditionnés, chaînés, etc. Les résultats des jobs peuvent être réutilisés par les jobs suivants (```set_stats```)
- les **logs** sont stockés (base de données posgres) de manière sécurisé et peuvent être exportés dans des outils d'analyse

[Ansible Tower Demo](https://www.ansible.com/products/tower/demo)
